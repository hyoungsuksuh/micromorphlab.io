Project Overview
================

Objective
---------

The overall objective of the Center is to simulate with quantified uncertainty, from
pore-particle-to-continuum-scales, a class of problems involving granular flows, large deformations,
and fracture/fragmentation of unbonded and bonded particulate materials. The overarching problem is the processing and thermo-mechanical behavior of compressed
virgin and recycled mock High Explosive (HE) material subjected to quasi-static and high-strain-rate confined and
unconfined compression, in-situ quasi-static X-ray computed tomography (CT), and dynamic (impact) experiments
with ultrafast and high-speed X-ray imaging at the Advanced Photon Source (APS), Argonne National
Laboratory (ANL).

.. figure:: length_time_plot_large_text_DNS_micro.png

Description
-----------

To accomplish the objective, a micromorphic multiphysics multiscale computational framework will be
developed, verified, and validated with quantified uncertainty, and executed on Exascale computing
platforms seamlessly through a scientific software workflow to reduce FTE effort on handling data
from beginning to end of simulation. Machine Learning (ML) algorithms will be applied to fill the
gaps in multiscale constitutive modeling via coordinated pore-particle-scale experiments and Direct
Numerical Simulations (DNS). An extensive, integrated, experimental program at quasi-static,
dynamic, and high-strain rates (some within the ultrafast high-speed X-ray imaging facility at the
APS and also pRad at Los Alamos National Laboratory, LANL), ranging from
pore-particle-to-specimen-scales, will be conducted to validate heterogeneous
pore-particle-to-continuum-scale computational models, calibrate model parameters, and validate the
overall computational framework. Exascale computing is needed to simulate these more sophisticated
micromorphic multiphysics bridged-DNS simulations, with offline ML training of micromorphic
constitutive relations to DNS. Furthermore, for Validation and Uncertainty Quantification (UQ)
requiring multiple instances of these simulations over statistical distributions of inputs (such as
particle size distribution), with high and low fidelity, Exascale computing is a necessity.

Anticipated Outcomes and Benefits of Research
---------------------------------------------

The Center research will usher in a new era of higher fidelity multiscale multiphysics computation
through large deformation micromorphic continuum field theories informed by DNS through the latest
ML techniques calibrated and validated against a rich experimental data set. Applying advances in
V&V/UQ, Exascale computing, and Integration/Workflows will make the applicability of such approach
to reduce uncertainty in continuum-scale computations based on statistical distributions of
materials information at the pore-particle-scale of bonded particulate materials a reality, which has significant influence on the success of the Stockpile Stewardship Program (e.g., High Explosive (HE) materials) and beyond.
